package com.levifralex.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Estudiante {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private Integer idEstudiante;

	@Column(nullable = false, length = 50)
	private String nombres;

	@Column(nullable = false, length = 50)
	private String apellidos;

	@Column(nullable = false, length = 8)
	private String dni;

	@Column(nullable = false)
	private int edad;

}
