package com.levifralex.service;

import java.util.stream.Stream;

import com.levifralex.model.Estudiante;

public interface IEstudianteService extends ICRUD<Estudiante, Integer> {
	
	Stream<Estudiante> getStudentsOrderedByAge();

}
