package com.levifralex.service.impl;

import java.util.Comparator;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.levifralex.model.Estudiante;
import com.levifralex.repo.IEstudianteRepo;
import com.levifralex.repo.IGenericRepo;
import com.levifralex.service.IEstudianteService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class EstudianteServiceImpl extends CRUDImpl<Estudiante, Integer> implements IEstudianteService {

	private final IEstudianteRepo repo;

	@Override
	protected IGenericRepo<Estudiante, Integer> getRepo() {
		return repo;
	}

	@Override
	public Stream<Estudiante> getStudentsOrderedByAge() {
		return repo.findAll().stream().sorted(Comparator.comparing(Estudiante::getEdad).reversed());
	}

}
