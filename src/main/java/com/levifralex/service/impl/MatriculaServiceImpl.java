package com.levifralex.service.impl;

import static java.util.stream.Collectors.groupingBy;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.levifralex.model.Matricula;
import com.levifralex.model.MatriculaDetalle;
import com.levifralex.repo.IGenericRepo;
import com.levifralex.repo.IMatriculaRepo;
import com.levifralex.service.IMatriculaService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class MatriculaServiceImpl extends CRUDImpl<Matricula, Integer> implements IMatriculaService {

	private final IMatriculaRepo repo;

	@Override
	protected IGenericRepo<Matricula, Integer> getRepo() {
		return repo;
	}

	@Override
	public Map<Object, Set<Object>> getCourseRegisteredAndStudents() {

		Stream<Matricula> saleStream = repo.findAll().stream();
		Stream<List<MatriculaDetalle>> lsStream = saleStream.map(Matricula::getDetallesMatricula);

		Stream<MatriculaDetalle> streamDetail = lsStream.flatMap(Collection::stream);

		return streamDetail.collect(groupingBy(d -> d.getCurso().getNombre(),
				Collectors.mapping(d -> d.getMatricula().getEstudiante().getNombres(), Collectors.toSet())));

	}

}
