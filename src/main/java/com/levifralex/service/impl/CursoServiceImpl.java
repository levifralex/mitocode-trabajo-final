package com.levifralex.service.impl;

import org.springframework.stereotype.Service;

import com.levifralex.model.Curso;
import com.levifralex.repo.ICursoRepo;
import com.levifralex.repo.IGenericRepo;
import com.levifralex.service.ICursoService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CursoServiceImpl extends CRUDImpl<Curso, Integer> implements ICursoService {
	
	private final ICursoRepo repo;

	@Override
	protected IGenericRepo<Curso, Integer> getRepo() {
		return repo;
	}

}
