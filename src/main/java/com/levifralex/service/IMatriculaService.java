package com.levifralex.service;

import java.util.Map;
import java.util.Set;

import com.levifralex.model.Matricula;

public interface IMatriculaService extends ICRUD<Matricula, Integer> {

	Map<Object, Set<Object>> getCourseRegisteredAndStudents();
	
}
