package com.levifralex.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EstudianteDTO {

	private Integer idEstudiante;
	
	@NotNull
    @Size(min = 3, max = 50)
    private String nombres;
	
	@NotNull
    @Size(min = 3, max = 50)
    private String apellidos;
	
	@NotNull
    @Size(min = 8, max = 8)
    private String dni;
	
	@NotNull
    @Min(value = 1)
	@Max(value = 150)
    private short edad;
	
}
