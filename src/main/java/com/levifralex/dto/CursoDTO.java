package com.levifralex.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CursoDTO {

	private Integer idCurso;

	@NotNull
	@Size(min = 3, max = 50)
	private String nombre;

	@NotNull
	@Size(min = 1, max = 5)
	private String sigla;

	@NotNull
	private boolean estado;

}
