package com.levifralex.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MatriculaDetalleDTO {
	
	@JsonBackReference
    private MatriculaDTO matricula;
	
	@NotNull
    private CursoDTO curso;
	
	@NotNull
    @Size(min = 1, max = 10)
    private String aula;

}
