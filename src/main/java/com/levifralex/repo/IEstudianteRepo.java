package com.levifralex.repo;

import com.levifralex.model.Estudiante;

public interface IEstudianteRepo extends IGenericRepo<Estudiante, Integer> {

}
