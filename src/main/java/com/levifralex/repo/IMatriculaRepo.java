package com.levifralex.repo;

import com.levifralex.model.Matricula;

public interface IMatriculaRepo extends IGenericRepo<Matricula, Integer> {

}
