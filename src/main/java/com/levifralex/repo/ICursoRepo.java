package com.levifralex.repo;

import com.levifralex.model.Curso;

public interface ICursoRepo extends IGenericRepo<Curso, Integer> {

}
