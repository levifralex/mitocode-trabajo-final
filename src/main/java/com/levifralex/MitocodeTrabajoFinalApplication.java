package com.levifralex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MitocodeTrabajoFinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(MitocodeTrabajoFinalApplication.class, args);
	}

}
