package com.levifralex.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.levifralex.dto.CursoDTO;
import com.levifralex.dto.GenericResponse;
import com.levifralex.dto.GenericResponseRecord;
import com.levifralex.model.Curso;
import com.levifralex.service.ICursoService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/curso")
@RequiredArgsConstructor
public class CursoController {

	private final ICursoService service;
	private final ModelMapper modelMapper;

	@GetMapping
	public ResponseEntity<GenericResponseRecord<CursoDTO>> readAll() throws Exception {
		List<CursoDTO> list = service.readAll().stream().map(this::convertToDto).toList();

		return ResponseEntity.ok(new GenericResponseRecord<>(200, "success", new ArrayList<>(list)));
	}

	@GetMapping("/{id}")
	public ResponseEntity<GenericResponse<CursoDTO>> readById(@PathVariable("id") Integer id) throws Exception {
		CursoDTO dto = convertToDto(service.readById(id));

		return ResponseEntity.ok(new GenericResponse<>(200, "success", Arrays.asList(dto)));
	}

	@PostMapping
	public ResponseEntity<CursoDTO> save(@Valid @RequestBody CursoDTO dto) throws Exception {
		Curso obj = service.save(convertToEntity(dto));

		return new ResponseEntity<>(convertToDto(obj), HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<CursoDTO> update(@Valid @PathVariable("id") Integer id, @RequestBody CursoDTO dto)
			throws Exception {
		Curso obj = service.update(convertToEntity(dto), id);

		return ResponseEntity.ok(convertToDto(obj));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id) throws Exception {
		service.delete(id);

		return ResponseEntity.noContent().build();
	}

	private CursoDTO convertToDto(Curso obj) {
		return modelMapper.map(obj, CursoDTO.class);
	}

	private Curso convertToEntity(CursoDTO dto) {
		return modelMapper.map(dto, Curso.class);
	}

}
