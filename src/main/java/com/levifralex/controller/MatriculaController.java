package com.levifralex.controller;

import java.util.Map;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.levifralex.dto.MatriculaDTO;
import com.levifralex.model.Matricula;
import com.levifralex.service.IMatriculaService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/matricula")
@RequiredArgsConstructor
public class MatriculaController {

	private final IMatriculaService service;
	private final ModelMapper modelMapper;

	@PostMapping
	public ResponseEntity<MatriculaDTO> save(@Valid @RequestBody MatriculaDTO dto) throws Exception {
		Matricula obj = service.save(convertToEntity(dto));

		return new ResponseEntity<>(convertToDto(obj), HttpStatus.CREATED);
	}

	@GetMapping("/getCourseRegisteredAndStudents")
	public ResponseEntity<Map<Object, Set<Object>>> getCourseRegisteredAndStudents() throws Exception {

		Map<Object, Set<Object>> byCourse = service.getCourseRegisteredAndStudents();

		return ResponseEntity.ok(byCourse);

	}

	private MatriculaDTO convertToDto(Matricula obj) {
		return modelMapper.map(obj, MatriculaDTO.class);
	}

	private Matricula convertToEntity(MatriculaDTO dto) {
		return modelMapper.map(dto, Matricula.class);
	}

}
