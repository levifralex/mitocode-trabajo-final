package com.levifralex.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.levifralex.dto.EstudianteDTO;
import com.levifralex.dto.GenericResponse;
import com.levifralex.dto.GenericResponseRecord;
import com.levifralex.model.Estudiante;
import com.levifralex.service.IEstudianteService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/estudiante")
@RequiredArgsConstructor
public class EstudianteController {

	private final IEstudianteService service;
	private final ModelMapper modelMapper;

	@GetMapping
	public ResponseEntity<GenericResponseRecord<EstudianteDTO>> readAll() throws Exception {
		List<EstudianteDTO> list = service.readAll().stream().map(this::convertToDto).toList();

		return ResponseEntity.ok(new GenericResponseRecord<>(200, "success", new ArrayList<>(list)));
	}

	@GetMapping("/{id}")
	public ResponseEntity<GenericResponse<EstudianteDTO>> readById(@PathVariable("id") Integer id) throws Exception {
		EstudianteDTO dto = convertToDto(service.readById(id));

		return ResponseEntity.ok(new GenericResponse<>(200, "success", Arrays.asList(dto)));
	}

	@PostMapping
	public ResponseEntity<EstudianteDTO> save(@Valid @RequestBody EstudianteDTO dto) throws Exception {
		Estudiante obj = service.save(convertToEntity(dto));

		return new ResponseEntity<>(convertToDto(obj), HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<EstudianteDTO> update(@Valid @PathVariable("id") Integer id, @RequestBody EstudianteDTO dto)
			throws Exception {
		Estudiante obj = service.update(convertToEntity(dto), id);

		return ResponseEntity.ok(convertToDto(obj));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id) throws Exception {
		service.delete(id);

		return ResponseEntity.noContent().build();
	}

	@GetMapping("/getStudentsOrderedByAge")
	public ResponseEntity<GenericResponseRecord<EstudianteDTO>> getStudentsOrderedByAge() throws Exception {
		List<EstudianteDTO> list = service.getStudentsOrderedByAge().map(this::convertToDto).toList();

		return ResponseEntity.ok(new GenericResponseRecord<>(200, "success", new ArrayList<>(list)));
	}

	private EstudianteDTO convertToDto(Estudiante obj) {
		return modelMapper.map(obj, EstudianteDTO.class);
	}

	private Estudiante convertToEntity(EstudianteDTO dto) {
		return modelMapper.map(dto, Estudiante.class);
	}

}
